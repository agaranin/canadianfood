<?php
namespace App\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class CategoryType extends AbstractEnumType
{
    public const BREAKFAST = 'BREAKFAST';
    public const BRUNCH = 'BRUNCH';
    public const LUNCH = 'LUNCH';
    public const DINNER = 'DINNER';
    public const APPETIZER = 'APPETIZER';
    public const BREAD = 'BREAD';
    public const DESSERT = 'DESSERT';
    public const DRINK = 'DRINK';
    public const SALAD = 'SALAD';
    public const SOUP = 'SOUP';


    protected static $choices = [
        self::BREAKFAST => 'Breakfast',
        self::BRUNCH => 'Brunch',
        self::LUNCH => 'Lunch',
        self::DINNER => 'Dinner',
        self::APPETIZER => 'Appetizer',
        self::BREAD => 'Bread',
        self::DESSERT => 'Dessert',
        self::DRINK => 'Drink',
        self::SALAD => 'Salad',
        self::SOUP => 'Soup'
    ];
}
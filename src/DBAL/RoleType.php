<?php
namespace App\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class RoleType extends AbstractEnumType
{
    public const USER = 'ROLE_USER';
    public const ADMIN = 'ROLE_ADMIN';


    protected static $choices = [
        self::USER => 'ROLE_USER',
        self::ADMIN => 'ROLE_ADMIN'
    ];
}
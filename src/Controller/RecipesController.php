<?php

namespace App\Controller;

use App\Entity\Step;
use App\Entity\Photo;
use App\Entity\Recipe;
use App\Entity\Review;
use App\Entity\Ingredient;
use Doctrine\ORM\EntityManager;
use App\Repository\StepRepository;
use App\Repository\PhotoRepository;
use App\Repository\RecipeRepository;
use App\Repository\ReviewRepository;
use App\Repository\IngredientRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;



class RecipesController extends AbstractController
{
    /**
     * @Route("/", name="app_home", methods="GET")
     */
    public function index(RecipeRepository $recipeRepository, PhotoRepository $photoRepository): Response
    {
        $recipes = $recipeRepository->findBy([], ['id' => 'DESC']);
        $data = array();
        foreach ($recipes as $recipe) {
            $photo = $photoRepository->findByRecipe($recipe);
            array_push($data,['id' => $recipe->getId(), 'title' => $recipe->getTitle(), 'description' => $recipe->getDescription(), 'photo' => array_pop($photo)]); 
        }
        $recipes = $data;
        return $this->render('recipes/index.html.twig', ['recipes' => $recipes]);
    }


    /**
     * @Route("/recipes/create", name="app_recipe_create", methods="GET|POST")
     */
    public function create(Request $request, EntityManagerInterface $entityManager): Response
    {


        $form = $this->createFormBuilder()
        ->add('imageFile', VichImageType::class, [
            'label' => 'Image (JPG or PNG file)',
            'required' => false,
            'allow_delete' => true,
            'download_uri' => true,
            'image_uri' => true,
            'imagine_pattern' => 'squared_thumbnail_small',
            'allow_extra_fields' => true,
            'empty_data' => '',
            'asset_helper' => true,])
            ->add('title', TextType::class, [
                'required' => true
                ])
            ->add('description', TextareaType::class, [
                'required' => true
                ])
            ->add('ingredients', TextareaType::class, ['required' => false, 'attr' => array(
                'placeholder' => 'Put each ingredient on its own line. Example: Olive oil/1/tbs'
            )])
            ->add('steps', TextareaType::class, ['required' => false,'attr' => array(
                'placeholder' => 'Put each step on its own line.'
            )])
            ->add('prep_time', TextType::class, ['required' => false])
            ->add('cook_time', TextType::class, ['required' => false])
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $recipe = new Recipe;
            $data = $form->getData();
            $recipe->setTitle($data['title']);
            $recipe->setDescription($data['description']);
            $recipe->setPrepTime($data['prep_time']);
            $recipe->setCookTime($data['cook_time']);
            
            $entityManager->persist($recipe);
            $entityManager->flush();

            if (isset($data['imageFile'])) {
                $photo = new Photo;
                $photo->setImageFile($data['imageFile']);
                $photo->setRecipe($recipe);
                $entityManager->persist($photo);
                $entityManager->flush();
            }

            // get logged user
            $recipe->setUser($this->getUser());
          

            $ingredients = preg_split('/\r\n/', $data['ingredients']);
            foreach ($ingredients as $key => $value) {
                $ingSplit = preg_split('/\//', $value);
                $ingredient = new Ingredient;
                if (trim($ingSplit[0]) === "") {
                    continue;
                }
                $name = $ingSplit[0];
                $ingredient->setName(trim($name));
                $amount = isset($ingSplit[1]) ? $ingSplit[1] : $ingSplit[1] = NULL;
                $ingredient->setAmount(trim($amount));
                $measurement = isset($ingSplit[2]) ? $ingSplit[2] : $ingSplit[2] = NULL;
                $ingredient->setMeasurement(trim($measurement));
                $ingredient->setRecipe($recipe);
                $entityManager->persist($ingredient);
                $entityManager->flush();
            }

            $steps = preg_split('/\r\n/', $data['steps']);
            $number = 1;
            foreach ($steps as $key => $value) {
                $step = new Step;
                if (trim($value) === "") {
                    continue;
                }
                $step->setInstruction(trim($value));
                $step->setNumber($number++);
                $step->setRecipe($recipe);
                $entityManager->persist($step);
                $entityManager->flush();
            }

            //flash message 1-type 2-message
            $this->addFlash('success', 'Recipe was successfully created');

            return $this->redirectToRoute('app_home');
        }


        return $this->render('recipes/create.html.twig', ['recipeForm' => $form->createView()]);
    }

    /**
     * @Route("/recipes/{id<[0-9]+>}", name="app_recipes_display", methods="GET|POST")
     */
    public function display(Request $request, EntityManagerInterface $entityManager, Recipe $recipe, IngredientRepository $ingredientRepository, StepRepository $stepRepository, ReviewRepository $reviewRepository, PhotoRepository $photoRepository): Response
    {
        $ingredients = $ingredientRepository->findByRecipe($recipe);
        $steps = $stepRepository->findByRecipe($recipe);
        $reviews = $reviewRepository->findByRecipe($recipe);
        $photo = $photoRepository->findByRecipe($recipe);
        $photo = $photo[0];

        $reviewForm = $this->createFormBuilder()
            ->add('review', TextareaType::class)
            ->getForm();
        $reviewForm->handleRequest($request);

        if ($reviewForm->isSubmitted() && $reviewForm->isValid()) {
            $data = $reviewForm->getData();
            $review = new Review;
            $review->setMessage($data['review']);
            $review->setRating(5);
            $review->setRecipe($recipe);
            $review->setUser($recipe->getUser());
            $entityManager->persist($review);
            $entityManager->flush();
            //flash message 1-type 2-message
            $this->addFlash('success', 'Review was successfully added');
            return $this->redirectToRoute('app_recipes_display', ['id' => $recipe->getId()]);
        }

        return $this->render('recipes/display.html.twig', ['reviewForm' => $reviewForm->createView(), 'recipe' => $recipe, 'ingredients' => $ingredients, 'steps' => $steps, 'reviews' => $reviews, 'photo' => $photo]);
    }
    /**
     * @Route("/recipes/{id<[0-9]+>}/update", name="app_recipies_update", methods="GET|POST")
     */
    public function update(Request $request, Recipe $recipe, EntityManagerInterface $entityManager, IngredientRepository $ingredientRepository, StepRepository $stepRepository, PhotoRepository $photoRepository): Response
    {
        $ingredients = $ingredientRepository->findByRecipe($recipe);
        $photoArr = $photoRepository->findByRecipe($recipe);
        $photo = null;
        if(isset($photoArr[0])){
            $photo = $photoArr[0];
        }
        //dd($photo);
        $ingrStr = "";
        $i = count($ingredients);
        foreach ($ingredients as $key => $value) {

            $ingrStr = $ingrStr . $value->getName();
            if( $value->getAmount() !== ""){
                $ingrStr = $ingrStr .'/' . $value->getAmount();
            }
            if( $value->getMeasurement() !== ""){
                $ingrStr = $ingrStr .'/' . $value->getMeasurement();
            }
            
            if ((--$i)) {
                $ingrStr = $ingrStr . "\r\n";
            }
        }

        $steps = $stepRepository->findByRecipe($recipe);
        $stepsStr = "";
        $i = count($steps);
        foreach ($steps as $key => $value) {

            $stepsStr = $stepsStr . $value->getInstruction();
            if ((--$i)) {
                $stepsStr = $stepsStr . "\r\n";
            }
        }
    
        $defaultData = [
            //'imageFile' => $photo,
            'title' => $recipe->getTitle(),
            'description' =>  $recipe->getDescription(),
            'ingredients' => $ingrStr,
            'steps' => $stepsStr,
            'prep_time' => $recipe->getPrepTime(),
            'cook_time' => $recipe->getCookTime()
        ];

        $photoForm = $this->createFormBuilder($photo)
        ->add('imageFile', VichImageType::class, [
            'label' => 'Image (JPG or PNG file)',
            'required' => false,
            'allow_delete' => true,
            'delete_label' => 'delete',
            'download_label' => 'download',
            'imagine_pattern' => 'squared_thumbnail_small',
            'download_uri' => true,
            'image_uri' => true,
            'asset_helper' => true,
            ])
            ->getForm();
            //$photoForm->handleRequest($request);

        $form = $this->createFormBuilder($defaultData)
            ->add('title', TextType::class)
            ->add('description', TextareaType::class)
            ->add('ingredients', TextareaType::class)
            ->add('steps', TextareaType::class)
            ->add('prep_time', TextType::class)
            ->add('cook_time', TextType::class)
            ->getForm();

        $form->handleRequest($request);
        //$entityManager->flush();

        if ($form->isSubmitted() && $form->isValid()) {

            foreach ($ingredients as $key => $value) {
                $entityManager->remove($value);
            }
            foreach ($steps as $key => $value) {
                $entityManager->remove($value);
            }

            $data = $form->getData();
            $recipe->setTitle($data['title']);
            $recipe->setDescription($data['description']);
            $recipe->setPrepTime($data['prep_time']);
            $recipe->setCookTime($data['cook_time']);
            $entityManager->flush();

            $ingredients = preg_split('/\r\n/', $data['ingredients']);
            foreach ($ingredients as $key => $value) {
                $ingSplit = preg_split('/\//', $value);
                $ingredient = new Ingredient;
                if (trim($ingSplit[0]) === "") {
                    continue;
                }
                $name = $ingSplit[0];
                $ingredient->setName(trim($name));
                $amount = isset($ingSplit[1]) ? $ingSplit[1] : $ingSplit[1] = NULL;
                $ingredient->setAmount(trim($amount));
                $measurement = isset($ingSplit[2]) ? $ingSplit[2] : $ingSplit[2] = NULL;
                $ingredient->setMeasurement(trim($measurement));
                $ingredient->setRecipe($recipe);
                $entityManager->persist($ingredient);
                $entityManager->flush();
            }

            $steps = preg_split('/\r\n/', $data['steps']);
            $number = 1;
            foreach ($steps as $key => $value) {
                $step = new Step;
                if (trim($value) === "") {
                    continue;
                }
                $step->setInstruction(trim($value));
                $step->setNumber($number++);
                $step->setRecipe($recipe);
                $entityManager->persist($step);
                $entityManager->flush();
            }

            //$entityManager->flush();
            //flash message 1-type 2-message
            $this->addFlash('success', 'Recipe was successfully updated');
            return $this->redirectToRoute('app_home');
        }


        return $this->render('recipes/update.html.twig', ['recipe' => $recipe, 'recipeForm' => $form->createView(), 'photoForm' => $photoForm->createView()]);
    }
    /**
     * @Route("/recipes/{id<[0-9]+>}/delete", name="app_recipies_delete", methods={"POST"})
     */
    public function delete(Request $request, Recipe $recipe, EntityManagerInterface $entityManager, IngredientRepository $ingredientRepository, StepRepository $stepRepository, ReviewRepository $reviewRepository,PhotoRepository $photoRepository): Response
    {

        if ($this->isCsrfTokenValid('recipe_delete_' . $recipe->getId(), $request->request->get('csrf_token'))) {
            $ingredients = $ingredientRepository->findByRecipe($recipe);
            $steps = $stepRepository->findByRecipe($recipe);
            $reviews = $reviewRepository->findByRecipe($recipe);
            $photos = $photoRepository->findByRecipe($recipe);
            // dd($photos);

            foreach ($ingredients as $key => $value) {
                $entityManager->remove($value);
            }
            foreach ($steps as $key => $value) {
                $entityManager->remove($value);
            }
            foreach ($reviews as $key => $value) {
                $entityManager->remove($value);
            }
            foreach ($photos as $key => $value) {
                $entityManager->remove($value);
            }
            

            $entityManager->remove($recipe);
            $entityManager->flush();
        }
        //flash message 1-type 2-message
        $this->addFlash('success', 'Recipe was successfully deleted');
        return $this->redirectToRoute('app_home');
    }
}

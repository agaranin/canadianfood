<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210216034326 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE category CHANGE recipe_id recipe_id INT DEFAULT NULL, CHANGE name name ENUM(\'BREAKFAST\', \'BRUNCH\', \'LUNCH\', \'DINNER\', \'APPETIZER\', \'BREAD\', \'DESSERT\', \'DRINK\', \'SALAD\', \'SOUP\') NOT NULL COMMENT \'(DC2Type:CategoryType)\'');
        $this->addSql('ALTER TABLE ingridient CHANGE recipe_id recipe_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE photo CHANGE recipe_id recipe_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE recipe CHANGE user_Id user_Id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE review CHANGE recipe_id recipe_id INT DEFAULT NULL, CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE step CHANGE recipe_id recipe_id INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE category CHANGE recipe_id recipe_id INT NOT NULL, CHANGE name name VARCHAR(200) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_general_ci`');
        $this->addSql('ALTER TABLE ingridient CHANGE recipe_id recipe_id INT NOT NULL');
        $this->addSql('ALTER TABLE photo CHANGE recipe_id recipe_id INT NOT NULL');
        $this->addSql('ALTER TABLE recipe CHANGE user_Id user_Id INT NOT NULL');
        $this->addSql('ALTER TABLE review CHANGE recipe_id recipe_id INT NOT NULL, CHANGE user_id user_id INT NOT NULL');
        $this->addSql('ALTER TABLE step CHANGE recipe_id recipe_id INT NOT NULL');
    }
}

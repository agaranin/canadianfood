<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
<!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">

<nav class="navbar navbar-expand-sm navbar-light bg-light mb-4" aria-label="Third navbar example">
  <div class="container">
    <div class="container-fluid">
      <a class="navbar-brand logo" href="{{ path('app_home') }}">
        <h2>
          <img src="{{asset('images/logo2-bg.png') }}" width="80" alt="logo" />
          <span class="font-weight-bold text-danger align-middle">Canadian food</span>
        </h2>
      </a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample03" aria-controls="navbarsExample03" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
    </div>
    <div class="collapse navbar-collapse" id="navbarsExample03">
      <ul class="navbar-nav me-auto mb-2">
        {% if app.user %}
        <li class="nav-item">

          <a class="nav-link" aria-current="page" href="{{ path('app_recipe_create')}}">CREATE</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">ACCOUNT</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#" onclick="event.preventDefault(); document.getElementById('js-logout').submit()" ;>LOGOUT</a>
        </li>
        <form id="js-logout" action="{{ path('app_logout') }}" method="POST" style="display: none;">
          <input name="csrf_token" type="hidden" value="{{ csrf_token('logout')}}">
        </form>
        {% else %}
        <li class="nav-item">
          <a class="nav-link" href="{{path('app_register')}}">REGISTER</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{path('app_login')}}">LOGIN</a>
        </li>
        {% endif %}
      </ul>
    </div>
  </div>
</nav>
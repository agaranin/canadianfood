-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: db1food.c27t15nlfoth.us-east-1.rds.amazonaws.com
-- Generation Time: Feb 24, 2021 at 07:04 AM
-- Server version: 8.0.20
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db1food`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int NOT NULL,
  `recipe_id` int DEFAULT NULL,
  `name` enum('BREAKFAST','BRUNCH','LUNCH','DINNER','APPETIZER','BREAD','DESSERT','DRINK','SALAD','SOUP') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:CategoryType)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `doctrine_migration_versions`
--

CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20210215161953', '2021-02-15 16:24:18', 360),
('DoctrineMigrations\\Version20210215204628', '2021-02-15 20:49:12', 189),
('DoctrineMigrations\\Version20210216014514', '2021-02-16 01:45:49', 1032),
('DoctrineMigrations\\Version20210216020429', '2021-02-16 02:04:37', 123),
('DoctrineMigrations\\Version20210216034326', '2021-02-16 03:43:38', 918),
('DoctrineMigrations\\Version20210218213024', '2021-02-18 21:33:08', 2193),
('DoctrineMigrations\\Version20210220022600', '2021-02-20 03:28:15', 292),
('DoctrineMigrations\\Version20210220034718', '2021-02-20 03:49:17', 142),
('DoctrineMigrations\\Version20210223022003', '2021-02-23 03:23:58', 435),
('DoctrineMigrations\\Version20210224032530', '2021-02-24 04:26:16', 311);

-- --------------------------------------------------------

--
-- Table structure for table `ingredients`
--

CREATE TABLE `ingredients` (
  `id` int NOT NULL,
  `recipe_id` int DEFAULT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `measurement` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ingredients`
--

INSERT INTO `ingredients` (`id`, `recipe_id`, `name`, `amount`, `measurement`) VALUES
(50, 67, 'yellow onions', '1 1', '4 pounds(about 2 medium plus 1 large)'),
(51, 67, 'Broccoli', '2', 'pounds'),
(52, 67, 'Kosher salt', '', ''),
(53, 67, 'Sharp cheddar, grated', '8 ounces', ''),
(69, 68, 'Soy sauce', '1', '2'),
(70, 68, 'Dark soy sauce (or an additional 2 teaspoons soy sauce)', '1', 'tablespoon'),
(71, 68, 'Salt', '1', '2'),
(72, 68, 'White pepper', '1', '2'),
(73, 68, 'Toasted sesame oil', '2', 'tablespoons'),
(74, 68, 'Scallions, chopped', '4', ''),
(75, 68, 'Fresh ginger, peeled and thinly sliced', '1 (2-inch)', ''),
(76, 68, 'Pounds bone-in, skin-on chicken pieces', '2', ''),
(77, 68, 'Pineapple & Bok Choy', '', ''),
(78, 68, 'Baby bok choy, halved', '3', 'small heads'),
(79, 68, 'Neutral oil, such as grapeseed', '2', 'tablespoons'),
(80, 68, 'Salt', '1', '4'),
(81, 68, 'Fresh pineapple chunks', '2', 'cups'),
(82, 68, 'Scallions, chopped', '', ''),
(83, 68, 'Crispy chile oil', '', ''),
(95, 70, 'Buttermilk', '2', '3'),
(96, 70, 'Unsalted butter', '4', 'tablespoons'),
(97, 70, 'Almond paste', '80', 'grams'),
(98, 70, 'Brown sugar (or granulated sugar)', '1', 'tablespoon'),
(99, 70, 'Flour', '1', 'cup'),
(100, 70, 'Baking powder', '1', 'teaspoon'),
(101, 70, 'Pinch of salt', '', ''),
(102, 70, '2 eggs', '', ''),
(103, 71, 'Chicken tenders or boneless skinless chicken breast', '1', 'pound'),
(104, 71, 'Panko bread crumbs', '1 1', '2'),
(105, 71, 'Grated parmesan cheese', '1', 'cup'),
(106, 71, 'Flour', '1', '4'),
(107, 71, 'Garlic salt', '1', '2'),
(108, 71, 'Eggs', '2', ''),
(109, 71, 'Salt and pepper', '', ''),
(110, 71, 'Vegetable oil', '', ''),
(122, 73, 'Dried beans, such as cannellini, pinto, or chickpeas', '1', 'pound'),
(123, 73, 'Olive oil, plus more, for serving', '5', 'tablespoons'),
(124, 73, 'Onion (red, yellow, whatever you have) 1 large  or 2 large shallots', '', ''),
(125, 73, 'Garlic, halved crosswise (don’t worry about the skins!)', '1', 'head'),
(126, 73, 'Lemon, halved crosswise', '1', ''),
(127, 73, 'Fresh chile (halved and seeded if preferred) or whole dried chile, optional', '1', ''),
(128, 73, 'Kosher salt, plus more for seasoning if necessary', '1', '4'),
(129, 73, 'Freshly ground black pepper', '', ''),
(130, 73, 'Rind from Parmesan cheese, optional', '', ''),
(131, 73, 'Sturdy greens, such as kale, broccoli rabe, or escarole', '1', 'head'),
(132, 73, 'Red wine vinegar', '1 to 2', 'tablespoons'),
(133, 73, 'Crusty bread, toasted (and rubbed with a clove of raw garlic if you have one), for serving', '', ''),
(134, 66, 'Romaine salad', '1', 'head'),
(135, 66, 'Scallions, finely sliced', '6', 'medium'),
(136, 66, 'Chopped fresh dill', '½', 'cup'),
(137, 66, 'Olive oil', '4', 'tablespoons'),
(138, 66, 'Red wine vinegar', '2', 'tablespoons'),
(139, 66, 'Salt and freshly ground black pepper to taste', '', ''),
(140, 69, 'Linguine', '12', 'ounces'),
(141, 69, 'Cherry or grape tomatoes, halved or quartered if large', '12', 'ounces'),
(142, 69, 'Onion, thinly sliced (about 2 cups)', '1', ''),
(143, 69, 'Cloves of garlic', '4', 'thinly sliced'),
(144, 69, 'Red pepper flakes', '1', '2'),
(145, 69, 'Basil, plus torn leaves for garnish', '2', 'sprigs'),
(146, 69, 'Extra-virgin olive oil, plus more for serving', '2', 'tablespoons'),
(147, 69, 'Coarse salt', '', ''),
(148, 69, 'Freshly ground black pepper', '', ''),
(149, 69, 'Water', '4 1', '2'),
(150, 69, 'Freshly grated Parmesan cheese, for serving', '', ''),
(151, 72, 'Fresh goat cheese, at room temperature', '11', 'ounces'),
(152, 72, 'Whipped cream cheese', '3', 'tablespoons'),
(153, 72, 'Prosciutto (2 for the dip, 1 to snack on)', '3', 'slices'),
(154, 72, 'Pomegranate seeds', '1 1', '2'),
(155, 72, 'Honey', '2', 'tablespoons'),
(156, 72, 'Juice from half a lemon', '', ''),
(157, 72, 'Finely grated lemon zest', '1', '2'),
(158, 72, 'Extra-virgin olive oil', '2', 'tablespoons'),
(159, 72, 'Kosher salt and freshly ground black pepper, to taste', '', ''),
(160, 72, 'Fresh thyme, to garnish', '', ''),
(161, 72, 'Fresh baguette, for serving', '', ''),
(162, 65, 'all-purpose flour', '1 ¾', 'cups'),
(163, 65, 'dark cocoa powder (such as Hershey\'s® Special Dark)', '1', 'cup'),
(164, 65, 'baking soda', '2', 'teaspoon'),
(165, 65, 'salt', '½', 'teaspoon'),
(166, 65, 'firmly packed dark brown sugar', '1', 'cup'),
(167, 65, 'white sugar', '½', 'cup'),
(168, 65, 'unsalted butter, softened', '½', 'cup'),
(169, 65, 'eggs at room temperature', '2', 'large'),
(170, 65, 'vanilla extract', '2', 'teaspoons'),
(171, 65, 'strong brewed coffee, at room temperature', '1', 'cup'),
(172, 65, 'buttermilk at room temperature', '1', 'cup');

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE `photos` (
  `id` int NOT NULL,
  `recipe_id` int DEFAULT NULL,
  `path` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `image_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_original_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_mime_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_size` int DEFAULT NULL,
  `image_dimensions` longtext COLLATE utf8mb4_unicode_ci COMMENT '(DC2Type:simple_array)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `photos`
--

INSERT INTO `photos` (`id`, `recipe_id`, `path`, `updated_at`, `image_name`, `image_original_name`, `image_mime_type`, `image_size`, `image_dimensions`) VALUES
(13, 65, 'cake-60345a3a821b0044784259.jpg', '2021-02-23 02:28:26', NULL, NULL, NULL, NULL, NULL),
(14, 66, 'maroulosalata-greek-romaine-salad-6035afa6ab4e6222890899.jpg', '2021-02-24 02:45:10', NULL, NULL, NULL, NULL, NULL),
(15, 67, 'crispy-broccoli-cheese-soup-6035b22025987542194341.webp', '2021-02-24 02:55:44', NULL, NULL, NULL, NULL, NULL),
(16, 68, 'mom-s-soy-sauce-chickenwebp-6035b48af1f28328777682.webp', '2021-02-24 03:06:03', NULL, NULL, NULL, NULL, NULL),
(17, 69, 'martha-stewart-s-one-pan-pasta-6035b5f16ff5f558050391.jpg', '2021-02-24 03:12:01', NULL, NULL, NULL, NULL, NULL),
(18, 70, 'almond-paste-waffles-6035b6f0be489009765095.webp', '2021-02-24 03:16:16', NULL, NULL, NULL, NULL, NULL),
(19, 71, 'bff-crispy-coated-chicken-6035b85b53df7165192216.jpg', '2021-02-24 03:22:19', NULL, NULL, NULL, NULL, NULL),
(20, 72, 'lemony-whipped-goat-cheese-with-crispy-prosciutto-and-pomegranate-6035b92a0133c501962820.webp', '2021-02-24 03:25:46', NULL, NULL, NULL, NULL, NULL),
(21, 73, 'a-pot-of-beans-and-greens-6035bb63e49cb689717918.webp', '2021-02-24 03:35:15', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `recipes`
--

CREATE TABLE `recipes` (
  `id` int NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prep_time` int DEFAULT NULL,
  `cook_time` int DEFAULT NULL,
  `user_Id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `recipes`
--

INSERT INTO `recipes` (`id`, `title`, `description`, `prep_time`, `cook_time`, `user_Id`) VALUES
(65, 'Dark Chocolate Cake', 'This super moist dark chocolate mousse cake combines unsweetened natural cocoa powder and dark cocoa powder for an extra rich flavor. Fill the cake with a simplified chocolate mousse and cover the whole dessert with semi-sweet chocolate ganache. If needed, you can prepare the ganache and mousse ahead of time.', 35, 25, 2),
(66, 'Maroulosalata', 'In Greece, maroulosalata is one of the most common salads. The simple ingredient list shouldn\'t fool you--it\'s such a light, crunchy, refreshing salad that is full of flavor. I particularly love it alongside roast or grilled meats. The key to authenticity is to almost shred the romaine lettuce (marouli), rather than tear it into large pieces as we normally do in the States.', 15, 10, 3),
(67, 'Crispy Broccoli Cheese Soup', 'How to make the broccoli-iest, cheesiest broccoli cheese soup? Skip the overcrowded ingredient list and use broccoli and the cheese multiple ways. That means no fluff like carrot, celery, potato, garlic, flour, cornstarch, vegetable or chicken stock, milk or cream or half-and-half, hot sauce, paprika, mustard, cayenne, or nutmeg. Just lots of broccoli, funky cheddar, and yellow onion. Here’s the game plan: You’ll purée blanched broccoli (including those starchy stalks!), while roasting even more broccoli in the oven for a crispy topping. Likewise, grated cheddar does double duty: yielding a creamy-silky soup and turning those broccoli florets into crackly-edged wonders. (For what it’s worth, that cheesy roasted broccoli is a recipe in itself—one that you could make on its own, pair with some warm bread and vinegary salad, and call it dinner.) Our third and final ingredient, sautéed onions, round out the flavor and add body. Because they end up whooshed in the blender, they serve the same savory role as vegetable stock: supporting, elevating, celebrating the broccoli and cheese. —Emma Laperruque', 20, 45, 3),
(68, 'Mom\'s Soy Sauce Chicken', 'A reddish-stained, soy sauce–marinated chicken is one that’s infused with loads of salty, fermented umami—and when you roast it with the skin on, you get even more umami from the crispy skin. These flavor bombs were invented by my mom and eight zillion other people who also know that soy sauce and chicken just go together. It’s not really fair to give her full credit, but I will anyway. Here, instead of grilling, we’re throwing them on a sheet pan to crisp alongside some bok choy, which, in my mom’s kitchen, is commonly braised. In Taiwan, where my mom is from, pineapples are plentiful, and they’re used in everything from chicken soups to fermented sauces. One key ingredient here is dark soy sauce, a more intense, concentrated type of soy sauce that will stain chicken a deeper shade of maroon when marinated overnight. You can find it in Asian groceries—or heap on some more light soy sauce if you can’t. Scoop this dish over a bowl of sticky rice, ham fried rice, or sesame noodles.', 80, 40, 3),
(69, 'Martha Stewart\'s Pasta', 'This pasta cooks entirely in one pan (without boiling water first) and makes its own sauce, all in about 9 minutes. Adapted slightly from Martha Stewart Living (June 2013) —Genius Recipes', 10, 20, 3),
(70, 'Almond Paste Waffles', 'These waffles have further convinced me almond paste makes all baked goods better. They taste like classic buttermilk waffles (crispy on the outside, light and airy on the inside) but with a little extra nuttiness and sweetness from the almond paste. They freeze beautifully, too. —Posie (Harwood) Brien', 15, 25, 3),
(71, 'BFF Crispy Coated Chicken', 'I’m in the trenches everyday with a full-time job, a working husband, and kids in school. Sometimes it is a real struggle to cook dinner and pack everyone\'s lunches every day. When I’m in the weeds, I know I can turn to this dish...it has never let me down. Kids love it. Adults love it. It’s great hot out of the pan for dinner and still great packed for lunch the next day. Grown-ups get it sliced into a crisp green salad while kids can dip their room temp chicken into ketchup. I always make a double batch of this the night before a plane trip and pack the leftovers as sandwiches. There is nothing that can get me through the morning or a long plane flight like knowing I’m going to have a lunch date with my BFF! - monkeymom —monkeymom\r\n\r\nTEST KITCHEN NOTES\r\nWHO: Monkeymom is an avid Food52er, weekday warrior, and scientist.\r\nWHAT: Your new BFF. (Seriously.)\r\nHOW: Dredge pounded-thin chicken in egg, panko, and cheese; fry it up; put on salads and sandwiches all week long.\r\nWHY WE LOVE IT: Let\'s first talk about the chicken: how crispy it is, how satisfying it is, how the panko and cheese make the best crust we\'ve had in a long time. Now, onto the dressing: This is Caesar\'s elegant cousin that we can\'t stop making. Monkeymom\'s recipe is now in our weekly rotation—we bet it will become a part of yours, too. —The Editors', 15, 45, 3),
(72, 'Goat Cheese With Crispy', 'Fresh chèvre is one of my favorite cheeses to transform into a dip. The delicate and slightly tangy flavor profile makes for an excellent base to build upon. This recipe highlights these subtle notes, with an addition of salty and crispy prosciutto bits to brighten up the creaminess. It\'s easy to whip up for any occasion! —Marissa Mullen', 10, 12, 3),
(73, 'A Pot of Beans & Greens', 'A person whose opinion I greatly respect once wrote me a letter sharing a lot of nice things, among them noting my “deep, unabiding love of beans.” And let me tell you: Truer words have never been written. (I may have cried a little, too. It’s nice to be seen.)\r\n\r\nAs someone who’s dabbled in vegetarianism on and off for years, I’ve never met a bean I don’t like. They’re appropriately filling and, when seasoned well, deeply flavorful—not to mention a third of the cost of meat. While I’ve always been one to stir a can of beans into soup or use to top a tray of cheesy nachos, I really first got into making huge pots of beans—from dried, not canned—when I started living on my own in New York. I always had a job (or jobs), but let’s be honest: I’ve never stopped being on a budget.\r\n\r\nBack then, instead of going out to dinner (which was bound to cost a lot), I started inviting friends over to eat. And that dinner was, more often than not, a pot of beans. These days, I’m not having friends over, and while I yearn for a pandemic-free reality, I’m still eating on the cheap.\r\n\r\nIt varies depending on where you shop, of course, but this meal, this gorgeous pot of beans, is the opposite of pricey. A pound of dried beans—which can easily serve four people—typically costs under $2. And for $4, you can get some alliums, a lemon, and a head of greens (whatever’s on sale). I jazz them up with dried chile, olive oil, and salt from the pantry, plus a Parm rind from the freezer (free flavor; don’t toss ’em!). I think they’re best served with crusty bread, either bought fresh (about $4, and you won’t eat the whole thing tonight) or revived from the freezer.\r\n\r\nBelly-filling, yet easy on the bank account.\r\n\r\nAs for the beans themselves, you can of course adapt as you’d like. I tend to use cannellini, great northern, white lima, pinto, or chickpeas in a pot like this, but really, anything goes. Unless they’re very large, I rarely soak beans in advance (prehydrating overnight can shorten t', 10, 80, 3);

-- --------------------------------------------------------

--
-- Table structure for table `reset_password_request`
--

CREATE TABLE `reset_password_request` (
  `id` int NOT NULL,
  `user_id` int NOT NULL,
  `selector` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hashed_token` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `requested_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `expires_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reset_password_request`
--

INSERT INTO `reset_password_request` (`id`, `user_id`, `selector`, `hashed_token`, `requested_at`, `expires_at`) VALUES
(1, 3, 'MuQJEqSgN47HJDs00m8h', 'x1FrpxO2JrJYTkXujnxTTwg01KjNYfrbBxwhk/swU1A=', '2021-02-23 03:37:19', '2021-02-23 04:37:19'),
(4, 27, 'wAuQtS72eURWjDxP1Sfy', '8SbRgAd+OkeKcZswBpucFa85GW/TgEziWdirWuoZQgA=', '2021-02-23 23:54:36', '2021-02-24 00:54:36'),
(5, 29, '4i6SakeI6zNe9cwBd6kA', 'juZlgd3TPIcKD++Bu2ABPPtN/SosLPOVqvBCQHLjo6Q=', '2021-02-24 04:31:01', '2021-02-24 05:31:01');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int NOT NULL,
  `recipe_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `message` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rating` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `recipe_id`, `user_id`, `message`, `photo`, `rating`) VALUES
(2, 65, 2, 'Was great, moist and flavorful. Used regular cacao, not a specialty cacao. Used a 10x10 in pan, 25 min at 350 F. Really good. Kids loved it.', NULL, 5),
(3, 65, 2, 'I didn\'t have baking soda and used baking powder as a substitute. The cake turned out beautiful!', NULL, 5),
(4, 65, 2, 'I made this exactly as described. The cake was delicious but the frosting is sickeningly sweet. I\'ll likely make the cake again but find a less sweet frosting recipe.', NULL, 5),
(5, 65, 2, 'I didn\'t have baking soda and used baking powder as a substitute. The cake turned out beautiful!', NULL, 5);

-- --------------------------------------------------------

--
-- Table structure for table `steps`
--

CREATE TABLE `steps` (
  `id` int NOT NULL,
  `recipe_id` int DEFAULT NULL,
  `number` int NOT NULL,
  `instruction` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `steps`
--

INSERT INTO `steps` (`id`, `recipe_id`, `number`, `instruction`) VALUES
(36, 67, 1, 'Heat the oven to 450°F.'),
(37, 67, 2, 'Pour a glug of oil into a stockpot set over medium heat. Add the onions and a big pinch of salt. Cook, stirring occasionally and lowering the heat if needed, until the pieces are soft, translucent, and beginning to brown, about 12 minutes.'),
(38, 67, 3, 'Meanwhile, cut away the broccoli stalks’ tough exteriors. Chop all the stalks into bite-size pieces and add these to a bowl. Chop what’s left into bite-size florets; add half of these to the bowl and the other half to a rimmed sheet pan.'),
(39, 67, 4, 'When the onions are ready, add 6 cups of water to the pot, along with a couple pinches of salt. Increase the heat to bring the water to a boil.'),
(40, 67, 5, 'While the water is heating up, generously drizzle the broccoli florets on the sheet pan with oil and sprinkle with salt. Toss with your hands, then spread out the broccoli as much as possible. Roast for about 15 minutes, until the bottoms are deeply browned and the stalks are knife-tender.'),
(41, 67, 6, 'When the onion water reaches a boil, add the broccoli stalks and florets in the bowl. Boil until the water is golden in hue and the broccoli stalks are fork-tender, about 8 minutes.'),
(42, 67, 7, 'Use a slotted spoon or spider to transfer the onions and broccoli from the stockpot to a blender. Blend until smooth. Add all but 1 cup of grated cheddar (we’re using that in a bit) to the blender. Blend again. Add 2 cups of vegetable stock from the stockpot and purée again. You should have about 2 cups of stock remaining; add more to the blender if you want the soup thinner; otherwise, pour the leftover stock into a container and refrigerate or freeze for soupy situations down the road.'),
(43, 67, 8, 'Pour the puréed soup into the emptied stockpot. If needed, set over low heat to stay warm.'),
(44, 67, 9, 'When the roasted broccoli is ready, remove the sheet pan from the oven. Increase the oven to 475°F. Sprinkle the remaining 1 cup of grated cheese on top of the roasted broccoli florets. Return to the oven and roast until the cheese is lacey and deeply golden brown, about 5 minutes (you can rotate the sheet pan if needed for even cooking). Pull from the oven and let cool for a few minutes to slightly crisp.'),
(45, 67, 10, 'Divide the soup into 4 bowls and evenly divide the crispy-cheesy broccoli florets on top.'),
(50, 68, 1, 'Make the marinade: Combine the soy sauce, dark soy sauce, salt, white pepper, sesame oil, scallions, and ginger in an airtight container or zip-top bag. Add the chicken to the marinade, cover the container or seal the bag, and marinate in the refrigerator for 12 to 24 hours.'),
(51, 68, 2, 'Heat the oven to 425°F.'),
(52, 68, 3, 'Combine the baby bok choy, 1 tablespoon of the neutral oil, and the salt on a sheet pan and toss to coat. Arrange the bok choy cut-side down. Toss the pineapple chunks with the remaining 1 tablespoon neutral oil and scatter the pineapple around the pan.'),
(53, 68, 4, 'Remove the chicken pieces from the marinade, shaking off any excess, and arrange them on the sheet pan nestled among the bok choy and pineapple. Roast for 25 to 30 minutes, then check on the bok choy; flip them or remove them from the sheet pan if they’re sufficiently crisped. Rotate the pan and roast for another 10 minutes, until a kitchen thermometer inserted into the thickest part of a piece registers 160°F. Sprinkle with the scallions and crispy chile oil (if using) and serve.'),
(57, 70, 1, 'Add the milk and butter to a medium saucepan and cook over medium heat until the butter melts. Add the sugar and almond paste (it helps to cut it into small chunks first), and stir the almond paste into the hot milk/butter mixture until it is mostly dissolved (use a spatula to smoosh it around to help it dissolve).'),
(58, 70, 2, 'Add the flour, baking powder, salt, and eggs. Whisk well to combine, then set the batter aside for 10 minutes to rest while you heat your waffle iron.'),
(59, 70, 3, 'Cook the waffles according to the instructions for your waffle iron.'),
(60, 70, 4, 'Eat while nice and hot! Or let them cool and freeze them—frozen waffles are delicious in my opinion, or you can toast them and enjoy them hot.'),
(61, 71, 1, 'If using chicken breast, slice into pieces about 1/2-inch thick. Pound chicken tenders or slices between two pieces of saran wrap to 1/4-inch in thickness. Sprinkle with salt and pepper. Set aside.'),
(62, 71, 2, 'Get breading stations ready. Mix flour and garlic salt on a plate. Next, beat eggs in a shallow but wide bowl. Then mix together panko and parmesan in another shallow but wide bowl or plate. Put a clean plate at that end.'),
(63, 71, 3, 'With one hand coat a piece of chicken with flour mixture and then drop into egg mixture. Pick it up, coat both sides with egg then drop into panko/cheese mixture. Using your other hand, coat both sides with panko/cheese. Set on the clean plate and continue coating the rest of the chicken.'),
(64, 71, 4, 'Add enough vegetable oil to generously coat your frying pan (I use non-stick for this) and heat on medium high heat. Once hot, add enough chicken to fill the pan. Once the coating has turned golden brown flip each slice over. Add additional oil to make sure that the panko/cheese mixture can also brown evenly on that side. When both sides are nicely browned, remove to a plate with paper towels.'),
(65, 71, 5, 'The chicken can be served hot or room temperature. To reheat, heat in a 350° F oven or toaster for 5 minutes or until coating sizzles.'),
(66, 71, 6, 'Slice to serve in a green salad with Lemony Parmesan Dressing (or your favorite).'),
(67, 71, 7, 'Pack chicken pieces into lunch boxes with a small container of ketchup.'),
(68, 71, 8, 'Tuck pieces of chicken between two slices of your favorite toast with lettuce and tomatoes.'),
(73, 73, 1, 'Pick through the beans for any debris, then rinse well in a colander or strainer. Optional step (but recommended if using very large beans, like corona or gigante): Place the beans in a bowl and cover with filtered water by 2 inches. Cover, transfer to the refrigerator, and soak the beans overnight, or up to 12 hours. When you’re ready to cook, drain the water and rinse the beans.'),
(74, 73, 2, 'Heat the oil in a 5- or 6-quart Dutch oven or pot over medium-high. Quarter the onion (no need to peel, but you can if you want), keeping the root end intact; if using shallots, halve lengthwise. Gently place the onion, garlic, and lemon in the pot, cut side down. Cook until charred (you can do both sides of the onion if you want), about 5 to 8 minutes.'),
(75, 73, 3, 'Meanwhile, heat a kettle filled with water or bring a medium pot of water to a simmer over the stove.'),
(76, 73, 4, 'Add the beans and chile, if using, to the pot along with enough simmering water to raise the liquid to about 2 inches from the top of the pot, or cover the beans by about 5 inches (about 7-9 cups, depending on the size of your pot. Keep more simmering water on hand, and replenish if the water dips lower than 2 inches from the top of the pot—using preheated water helps keep the bean-cooking liquid hot throughout the pot, therefore making the beans cook faster.)'),
(77, 73, 5, 'Stir in the salt (yes, 1/4 cup is correct!) and several grinds of black pepper and bring the mixture to a boil. Reduce the heat to medium-low, or whatever heat your stove needs to keep the beans at a simmer.'),
(78, 73, 6, 'Stir in the Parmesan rind, if using, and half-cover the pot with a lid. Cook the beans for 30 minutes, then taste the liquid. Does it need more salt? Add more! Is it too salty? Add more water by the cup until it tastes right. If using a green with firm stems, strip the greens from their stems, chop the stems finely, and add (only the stems) to the pot now, as they are edible.'),
(79, 73, 7, 'Continue cooking for another 30 minutes, then taste a bean. If it’s at all crunchy, keep cooking, checking every 30 minutes until the beans are creamy all the way through, which can take anywhere from an additional hour to several hours, depending on the size and age of your beans.'),
(80, 73, 8, 'When the beans are cooked to your liking, turn off the heat. Find the halved head of garlic and use a spoon to smash the cloves against the side of the pot, then remove the spent root end of the garlic and discard. Scoop out the lemon halves, and Parmesan rind and spent dried chile if using, and discard. Roughly chop or tear the greens and stir into the mixture to gently wilt. Stir in 1 tablespoon of vinegar, adding more to taste.'),
(81, 73, 9, 'Spoon the beans and greens into bowls with plenty of broth and serve with a drizzle of olive oil, several grinds of black pepper, and toast.'),
(82, 66, 1, 'Wash and dry romaine lettuce. Lay leaves on a cutting board and slice lengthwise across the leaves, shredding them into 1/4-inch strips.'),
(83, 66, 2, 'Transfer lettuce to a large bowl. Add scallions, dill, olive oil, vinegar, salt, and pepper; toss until well combined. Adjust oil, vinegar, and seasoning to taste.'),
(84, 69, 1, 'Combine pasta, tomatoes, onion, garlic, red-pepper flakes, basil, oil, 2 teaspoons salt, 1/4 teaspoon pepper, and water in a large straight-sided skillet (the linguine should lay flat).'),
(85, 69, 2, 'Bring to a boil over high heat. Boil mixture, stirring and turning pasta frequently with tongs or a fork, until pasta is al dente and water has nearly evaporated, about 9 minutes.'),
(86, 69, 3, 'Season to taste with salt and pepper, divide among 4 bowls, and garnish with basil. Serve with olive oil and Parmesan.'),
(87, 72, 1, 'Heat eat oven to 400°F.'),
(88, 72, 2, 'Lay the slices of prosciutto on a foil or parchment-lined baking tray. Bake for 10 to 12 minutes until crispy but not burnt. Remove prosciutto from the oven and let cool; once cool, gently break into small bits.'),
(89, 72, 3, 'In the bowl of a food processor, pulse the goat cheese, whipped cream cheese, honey, and lemon until combined. Add one tablespoon of olive oil and blend until smooth. Taste and season with salt and pepper, as well as additional honey and/or lemon to taste. Transfer to a serving bowl.'),
(90, 72, 4, 'Top cheese with remaining tablespoon of olive oil, crispy prosciutto, pomegranate seeds, and fresh thyme. Enjoy with slices of fresh baguette.'),
(91, 65, 1, 'Preheat the oven to 350 degrees F (175 degrees C). Grease a 10x15-inch rimmed baking sheet and line the bottom of the pan with parchment paper.'),
(92, 65, 2, 'Whisk flour, cocoa powder, baking soda, and salt together in a bowl until thoroughly combined. Set aside.'),
(93, 65, 3, 'Beat brown sugar, white sugar, and butter together in a large bowl with an electric mixer until light and fluffy. Beat in eggs one at a time, mixing well after each addition. Mix in vanilla extract. Add in 1/3 of the flour mixture, and mix until just combined. Add in 1/2 of the coffee and buttermilk, and mix until just combined. Continue alternating additions of the flour mixture with coffee and buttermilk, mixing just until combined. Pour batter into the prepared pan; smooth into an even layer. Tap pan firmly on the counter several times to remove air bubbles.'),
(94, 65, 4, 'Bake in the preheated oven until a toothpick inserted into the center of the cake comes out with a few moist crumbs, about 25 minutes. Do not overbake. Place cake on a wire rack and allow to cool completely.'),
(95, 65, 5, 'Meanwhile, make the frosting. Beat butter in a large bowl until completely smooth. Mix in cocoa powder, espresso powder, and salt until thoroughly combined. Mix in vanilla. Add in powdered sugar 1 cup at a time, mixing well after each addition. Pour in heavy cream and beat until frosting is light and fluffy, 2 to 3 minutes. If frosting is too thick, add more heavy cream.'),
(96, 65, 6, 'Spread frosting over the cooled cake.');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` enum('ROLE_USER','ROLE_ADMIN') COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '(DC2Type:RoleType)',
  `is_verified` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `photo`, `email`, `role`, `is_verified`) VALUES
(2, 'Artem', '$2y$13$0gII04Hk1y8xSOs2GADs3ecll09dtaCRTVlnzWpKYj6UrFQO0XfiC', NULL, 'agaranin@me.com', '', 0),
(3, 'Vladimir', '$2y$13$qnyA0VMiKi.c.kcWDIBwrereULRLn4pFNk.Muoh.o1dlNfBFiHv6W', NULL, 'v@me.me', '', 0),
(21, 'Artem2', '$argon2id$v=19$m=65536,t=4,p=1$VFV5TS5KcGRiaTNoZmpEdQ$FMbN5KbD+ptC5OBDE3yWYIeDYQASZ1YXzChNJgKP1N4', NULL, 'agaranin2@me.com', '', 0),
(27, 'Vlad', '$argon2id$v=19$m=65536,t=4,p=1$SE5pODhLQkpSMHpSS0pNNA$5X+XmrbukOoeL3pJA+k6KvIt925znBQJrB2jdvvWTTc', NULL, 'vladimirtocarica@gmail.com', 'ROLE_USER', 0),
(28, 'Borea', '$argon2id$v=19$m=65536,t=4,p=1$bGFrcTlhcllJRnpXMm5nag$fAsTzoTpNrMBZBR8GMKzQ/2alEPxDxE2RUTaXJyRTDk', NULL, 'boris@me.me', NULL, 0),
(29, 'vova', '$argon2id$v=19$m=65536,t=4,p=1$OTlJdWgvMnY4Ylo0SlhFaA$Qh5cItNJzQpPi0yWUks8C2ss4k4rC4rL6A4lFstAHC4', NULL, 'donray@mail.ru', NULL, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `recipe_id` (`recipe_id`);

--
-- Indexes for table `doctrine_migration_versions`
--
ALTER TABLE `doctrine_migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `ingredients`
--
ALTER TABLE `ingredients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `recipe_id` (`recipe_id`);

--
-- Indexes for table `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `recipe_id` (`recipe_id`);

--
-- Indexes for table `recipes`
--
ALTER TABLE `recipes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_Id` (`user_Id`);

--
-- Indexes for table `reset_password_request`
--
ALTER TABLE `reset_password_request`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_7CE748AA76ED395` (`user_id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `recipe_id` (`recipe_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `steps`
--
ALTER TABLE `steps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `recipe_id` (`recipe_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ingredients`
--
ALTER TABLE `ingredients`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=173;

--
-- AUTO_INCREMENT for table `photos`
--
ALTER TABLE `photos`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `recipes`
--
ALTER TABLE `recipes`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `reset_password_request`
--
ALTER TABLE `reset_password_request`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `steps`
--
ALTER TABLE `steps`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `FK_3AF3466859D8A214` FOREIGN KEY (`recipe_id`) REFERENCES `recipes` (`id`);

--
-- Constraints for table `ingredients`
--
ALTER TABLE `ingredients`
  ADD CONSTRAINT `FK_8CC07B3B59D8A214` FOREIGN KEY (`recipe_id`) REFERENCES `recipes` (`id`);

--
-- Constraints for table `photos`
--
ALTER TABLE `photos`
  ADD CONSTRAINT `FK_876E0D959D8A214` FOREIGN KEY (`recipe_id`) REFERENCES `recipes` (`id`);

--
-- Constraints for table `recipes`
--
ALTER TABLE `recipes`
  ADD CONSTRAINT `FK_A369E2B532EAF737` FOREIGN KEY (`user_Id`) REFERENCES `users` (`id`);

--
-- Constraints for table `reset_password_request`
--
ALTER TABLE `reset_password_request`
  ADD CONSTRAINT `FK_7CE748AA76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `reviews`
--
ALTER TABLE `reviews`
  ADD CONSTRAINT `FK_6970EB0F59D8A214` FOREIGN KEY (`recipe_id`) REFERENCES `recipes` (`id`),
  ADD CONSTRAINT `FK_6970EB0FA76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `steps`
--
ALTER TABLE `steps`
  ADD CONSTRAINT `FK_34220A7259D8A214` FOREIGN KEY (`recipe_id`) REFERENCES `recipes` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
